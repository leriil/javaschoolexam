package com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     *                  "(|\\d+"
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement){
        // TODO: Implement the logic here
        boolean validEquation = true;
        String solution = null;

        if (isNotAnEquation(statement)||statement.isEmpty()||notEvenNumberOfBrackets(statement)) validEquation = false;

        if (validEquation) {
            try {
                ScriptEngineManager mgr = new ScriptEngineManager();
                ScriptEngine engine = mgr.getEngineByName("JavaScript");
                solution= engine.eval(statement).toString();
            }catch (ScriptException e){
                e.printStackTrace();
            }
            if(!solution.equals("Infinity")){
                return solution;
            }
            else return null;
        } else return null;
    }

    private boolean isNotAnEquation(String statement) {
        try {
            Pattern p = Pattern.compile(",|\\+{2,}|\\-{2,}|\\*{2,}|\\.{2,}|\\/{2,}");
            Matcher m = p.matcher(statement);
            return m.find();
        }catch (NullPointerException e){
            return true;
        }
    }

    private boolean notEvenNumberOfBrackets(String statement){
        int counter=0;
        boolean notEvenNumberOfBrackets=true;
        for (int i = 0; i < statement.length(); i++) {
            if(statement.charAt(i)=='('||statement.charAt(i)==')'){
                counter++;
            }
        }
        if(counter%2==0){notEvenNumberOfBrackets=false;}
        return notEvenNumberOfBrackets ;
    }
}

