package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        try{
            int xSize = x.size();
            int ySize = y.size();
            if (xSize > ySize) {
                return false;
            } else {
                if (isSubsequence(x, y, xSize, ySize)) {
                    return true;
                }
            }}catch (NullPointerException e){
            throw new IllegalArgumentException();
        }
        return false;
    }

    private boolean isSubsequence(List x, List y, int xSize, int ySize){
        if (xSize == 0)
            return true;
        if (ySize == 0)
            return false;
        if(x.get(xSize-1)==y.get(ySize-1)){
            return isSubsequence(x,y,xSize-1,ySize-1);
        }
        return isSubsequence(x,y,xSize,ySize-1);
    }
}
