package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        // TODO : Implement your solution here
        if (!isNumberTriangle(inputNumbers.size()) || !checkList(inputNumbers)) {
            throw new CannotBuildPyramidException();
        }
        int[][] pyramid;
        try {
            Collections.sort(inputNumbers);
            //calculate number of Elements
            int numOfElements = inputNumbers.size();
            //calculate number of rows
            int rows;
            for (rows = 1; rows < numOfElements; rows++) {
                numOfElements -= rows;
            }
            //  calculate number of columns
            int columns = rows * 2 - 1;
            //make pyramid
            pyramid = new int[rows][columns];
            //fill pyramid with zeroes
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    pyramid[i][j] = 0;
                }
            }
            numOfElements = inputNumbers.size();
            int zeroesInRow = 0;
            int numbersInRowLeft;
            for (int i = rows - 1; i >= 0; i--) {
                numbersInRowLeft = i + 1;
                for (int j = columns - zeroesInRow - 1; j >= 0; j -= 2) {
                    if (numbersInRowLeft > 0) {
                        pyramid[i][j] = inputNumbers.get(numOfElements - 1);
                        numOfElements--;
                        numbersInRowLeft--;
                    }
                }
                zeroesInRow++;
            }
        } catch (CannotBuildPyramidException | OutOfMemoryError r) {
            throw new CannotBuildPyramidException();
        }
        return pyramid;
    }

    //check if number is triangle
    private boolean isNumberTriangle(int input) {
        int TriangleNumber = 0;
        int n = 0;

        while (TriangleNumber < input) {
            TriangleNumber += n;
            n++;
        }
        if (TriangleNumber != input) {return false;}

        return true;
    }

    //check for nulls or illegal format of numbers
    private boolean checkList(List<Integer> inputNumbers) {
        try {
            for (Integer element : inputNumbers) {
                Integer.parseInt(element.toString());
            }
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
        return true;
    }
}



